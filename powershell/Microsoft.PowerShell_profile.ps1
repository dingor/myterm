# import plugin
Import-Module Terminal-Icons
Import-Module powercolorls

# remove system settings
Remove-Alias ls

# set env
$env:POWERSHELL_HOME          = 'D:\powershell'
$env:NEOVIM_HOME              = 'D:\Neovim'
$env:MINGW_HOME               = 'D:\mingw64'

# set PATH
$env:PATH = $env:PATH + ";$env:POWERSHELL_HOME"
$env:PATH = $env:PATH + ";$env:NEOVIM_HOME\bin"
$env:PATH = $env:PATH + ";$env:NEOVIM_HOME\cmd"

# set alias
Set-Alias -Name gui -Value $env:NEOVIM_HOME\\cmd\\gitui.exe
Set-Alias -Name vim -Value $env:NEOVIM_HOME\\bin\\nvim.exe
Set-Alias -Name vi -Value $env:NEOVIM_HOME\\bin\\nvim.exe
Set-Alias -Name ls -Value Get-ChildItem
Set-Alias -Name ll -Value Get-ChildItem

# quick cmd
function dd {
    Write-Host "⚙️ 进入桌面"
    Set-Location -Path "~/Desktop"
}

function vic {
    Write-Host "⚙️ 打开PowerShell配置"
    vim $HOME/Documents/PowerShell/Microsoft.PowerShell_profile.ps1
}

function vig {
    Write-Host "⚙️ 打开gitconfig配置"
    vim $HOME/.gitconfig
}

function vif {
    vim $(fzf --preview 'bat --style=numbers --color=always --line-range :500 {}') 
}

function cdf {
    cd $(fd -t d | fzf)
}
