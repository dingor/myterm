# MyTerm

个人终端配置，贼好用的那种！！！PS：个人用的Mac/Ubuntu，公司用的Windows

## 栗子

![0](./img/0.png)

![1](./img/1.png)

![2](./img/2.png)

![3](./img/3.png)

![4](./img/4.png)

![5](./img/5.png)

![6](./img/6.png)

![7](./img/7.png)

![8](./img/8.png)

## 安装与配置

内容较多，参考[Myterm配置]()

## 常用工具

**部分也是Neovim依赖, 建议都装上**

| 名称    | 作用         | 架构支持      |
| :-:     | :-:          | :-:           |
| gitui   | git终端管理  | win/mac/linux |
| bat     | 文件预览     | win/mac/linux |
| fd      | 文件查询     | win/mac/linux |
| fzf     | 文件查询     | win/mac/linux |
| ripgrep | 模糊搜索     | win/mac/linux |
| glow    | markdown渲染 | win/mac/linux |

## 编辑器 - Neovim

### 通用能力

- 首页
- 模糊搜索
- 文件树
- 状态栏与标签页定制美化
- Git改动提示（文件树、buffer）
- 注释（快速注释、注释生成）
- 基于treesistter的配色方案
- LSP与Linter错误展示（默认GO与Python，其他的可自行配置）
- 大纲
- 输入补全
- Debug

### 语言支持

主要基于`nvim-lspconfig`，具体配置参考该插件的文档。额外添加的话自己根据需要配置。

个人平时主要语言为`go + python + c/c++`，然后偶尔会用`vue + ts/js + css`写写前端
